package sy.com.android_batterycircle;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class BatteryCircleView extends View implements Runnable {

    private static final String TAG = "sy";

    // 新建两个画笔，一个是画外边的圆环，一个是中间的文字
    private Paint circlePaint;
    private Paint textPaint;
    // 底边画笔
    private Paint bgCirclePaint;
    // 小圆球画笔
    private Paint smallCirclePaint;

    private int viewWidth;
    private int viewheight;


    // 背景圆环线宽
    private float bgCircleLineWeight;

    // 背景圆环颜色
    private int bgCircleColor;


    // 小球半径
    private float smallBallRadius;

    // 小球颜色
    private int smallBallColor;


    // 圆环线宽
    private float circleLineWeight;

    // 圆环半径
    private float circleRadius;

    // 圆环起始值
    private int starAngle = -90;

    // 圆环旋转值
    private int sweepAngle = 0;

    // 电量值
    private int batteryVal = 100;

    // 默认画笔颜色
    private int initPaintColor = Color.BLACK;

    public BatteryCircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        // attr 属性设置
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.BatteryCircleView);

        // 圆环线宽
        circleLineWeight = typedArray.getDimension(R.styleable.BatteryCircleView_circleLineWeight, 30);

        // 圆环半径
        circleRadius = typedArray.getDimension(R.styleable.BatteryCircleView_circleRadius, 0);

        // 背景圆环线宽
        bgCircleLineWeight = typedArray.getDimension(R.styleable.BatteryCircleView_bgLineWeight, circleLineWeight + 5);
        // 背景圆环颜色
        bgCircleColor = typedArray.getColor(R.styleable.BatteryCircleView_bgLineColor, Color.GRAY);

        // 小球半径
        smallBallRadius = typedArray.getDimension(R.styleable.BatteryCircleView_smallBallRadius, 30);

        // 小球颜色
        smallBallColor = typedArray.getColor(R.styleable.BatteryCircleView_smallBallColor, Color.BLUE);

        typedArray.recycle();

        // 关闭硬件加速
        setLayerType(LAYER_TYPE_SOFTWARE, null);

        initPaint();
    }

    public void sendBatteryVal(int battery) {
        batteryVal = battery;
        /**
         * <9为红色
         * <60橘色
         * <100绿色
         */
        if (batteryVal < 10) {
            initPaintColor = Color.RED;
        } else if (batteryVal < 60) {
            initPaintColor = Color.YELLOW;
        } else if (batteryVal <= 100) {
            initPaintColor = Color.GREEN;
        }
    }


    private void initPaint() {
        // 初始化圆环画笔
        // 圆环(抗锯齿)
        circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // 圆环设置为描边
        circlePaint.setStyle(Paint.Style.STROKE);
        // 颜色
        circlePaint.setColor(initPaintColor);
        // 粗细 px
        circlePaint.setStrokeWidth(circleLineWeight);
        // 圆角
        circlePaint.setStrokeCap(Paint.Cap.ROUND);
        // 光晕效果
        BlurMaskFilter maskFilter = new BlurMaskFilter(10, BlurMaskFilter.Blur.SOLID);
        circlePaint.setMaskFilter(maskFilter);


        // 小圆球画笔
        smallCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // 小圆球设置为填充
        smallCirclePaint.setStyle(Paint.Style.FILL);
        // 颜色
        smallCirclePaint.setColor(smallBallColor);
        // 圆角
        smallCirclePaint.setStrokeCap(Paint.Cap.ROUND);
        // 光晕效果
        smallCirclePaint.setMaskFilter(maskFilter);


        // 初始化背景圆环
        bgCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // 圆环设置为描边
        bgCirclePaint.setStyle(Paint.Style.STROKE);
        // 颜色
        bgCirclePaint.setColor(bgCircleColor);
        // 粗细 px
        bgCirclePaint.setStrokeWidth(bgCircleLineWeight);
        // 圆角
        bgCirclePaint.setStrokeCap(Paint.Cap.ROUND);


        // 文字(抗锯齿)
        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // 文字横向居中
        textPaint.setTextAlign(Paint.Align.CENTER);
        // 文字加粗
        textPaint.setFakeBoldText(true);

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // 图像的中心点为：viewWidth / 2 , viewHeight / 2
        float x = viewWidth / 2;
        float y = viewheight / 2;

        /**
         * 如果没有设定圆环半径
         * 则圆环半径为  长宽中较短者 - 线宽
         */

        if (circleRadius == 0) {
            circleRadius = (y > x ? x : y) - circleLineWeight;
        }

        // 框
        RectF oval = new RectF(x - circleRadius, y - circleRadius,
                x + circleRadius, y + circleRadius);

        // 背景圆环
        canvas.drawArc(oval, starAngle, -360, false, bgCirclePaint);

        // 圆环
        circlePaint.setColor(initPaintColor);
        canvas.drawArc(oval, starAngle, sweepAngle, false, circlePaint);


        // 小圆球
        // 横坐标
        float smallCirclePaint_x = (float) (x - circleRadius * Math.sin(Math.toRadians(-sweepAngle)));
        // 纵坐标
        float smallCirclePaint_y = (float) (y - circleRadius * Math.cos(Math.toRadians(-sweepAngle)));
        // 绘小圆球
        canvas.drawCircle(smallCirclePaint_x, smallCirclePaint_y, smallBallRadius, smallCirclePaint);


        // 绘制文字
        textPaint.setTextSize(circleRadius / 2);
        canvas.drawText((int) (-sweepAngle / 3.6) + "%", x, y + circleRadius / 9, textPaint);

        textPaint.setTextSize(circleRadius / 4);
        canvas.drawText("电量", x, y + circleRadius / 2, textPaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
//        Log.e(TAG, "onMeasure widthMode: "+widthMode);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
//        Log.e(TAG, "onMeasure widthSize: "+widthSize);
//        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
//        Log.e(TAG, "onMeasure heightMode: "+heightMode);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
//        Log.e(TAG, "onMeasure heightSize: "+heightSize);


        viewWidth = widthSize;
        viewheight = heightSize;
    }

    @Override
    public void run() {
        // 电量对应的角度：
        int batteryAngle = -360 * batteryVal / 100;
        sweepAngle = 0;
        while (sweepAngle >= batteryAngle) {
            try {
                sweepAngle--;
                postInvalidate();
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
