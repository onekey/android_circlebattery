package sy.com.android_batterycircle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "sy";
    // 电量输入对话框
    private EditText et;
    // 按钮
    private Button btn;

    // 电量
    private BatteryCircleView cv;
    // 刷新电量线程
    private Thread batteryThread;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e(TAG, "onCreate: ");
        // 获得对象
        cv = findViewById(R.id.cv);
        // 电量输入框
        et = findViewById(R.id.et);
        // 确定按钮
        btn = findViewById(R.id.btn);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (batteryThread != null) {
                    try {
                        Log.e(TAG, "batteryThread: 销毁");
                        batteryThread.interrupt();
                    } catch (Exception e) {
                        Log.e(TAG, "interrupt error");
                    }
                }
                Thread th = new Thread(cv);
                String val = et.getText().toString().trim();
                cv.sendBatteryVal(Integer.parseInt(val));
                th.start();
                batteryThread = th;
            }
        });
    }
}
